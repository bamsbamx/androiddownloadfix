package com.anrapps.downloadfix;

import android.app.*;
import android.os.*;
import android.content.Intent;
import android.widget.TextView;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.view.View;
import android.content.Context;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;
import java.io.File;
import android.widget.Toast;

public class MainActivity extends Activity 
{

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		Uri intentUri = getIntent().getData();
		if (intentUri == null) {
			Toast.makeText(this, "Cant open file", Toast.LENGTH_SHORT).show();
			return;
		}
		
		Intent intent = new Intent(Intent.ACTION_VIEW);

		String filePath = getRealPathFromURI(intentUri);
		String fileName = new File(filePath).getName();
		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(getExtension(fileName));

		final Uri u = new Uri.Builder().scheme("file").path(filePath).build();
		intent.setDataAndType(u, mimeType);
		startActivity(intent);
		finish();
	}
	
	
	
	
//    @Override
//    protected void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main);
//		
//		TextView tv = (TextView) findViewById(R.id.mainTextView1);
//		final Intent i = getIntent();
////		StringBuilder sb = new StringBuilder();
////		sb.append("Action: " + i.getAction() + "\n");
////		sb.append("DataString: " + i.getDataString() + "\n");
////		sb.append("DataUri: " + i.getData().toString() + "\n");
////		//sb.append("Extras: " + i.getExtras().toString() + "\n");
////		sb.append("Path: " + i.getData().getPath() + "\n");
////		sb.append("Scheme: " + i.getData().getScheme() + "\n");
////		sb.append("(mime)Type: " + getContentResolver().getType(i.getData()) + "\n");
////		sb.append("Filename: " + getText(i.getData()) + "\n");
////		sb.append("Intent type: " + i.getType());
////		sb.append("\n" + "\n");
////		tv.setText(sb.toString());
//		
//		final Uri uri = i.getData();
//		if (uri != null) {
//			tv.setText(getRealPathFromURI(uri));
//		}
//		
//		Button b = (Button) findViewById(R.id.mainButton1);
//		b.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View p1) {
//					Intent intent = new Intent(Intent.ACTION_VIEW);
//					
//					String filePath = getRealPathFromURI(uri);
//					String fileName = new File(filePath).getName();
//					String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(getExtension(fileName));
//					
//					final Uri u = new Uri.Builder().scheme("file").path(getRealPathFromURI(uri)).build();
//					intent.setDataAndType(u, mimeType);
//					startActivity(intent);
//				}
//				
//			
//		});
//		
//		
//    }
	
	private String getRealPathFromURI(Uri contentUri) {
		Cursor cursor = null;
		try { 
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = getContentResolver().query(contentUri,  proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}
	
	private static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }
	
	private static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        }
        int extensionPos = filename.lastIndexOf(".");
        int lastSeparator = indexOfLastSeparator(filename);
        return (lastSeparator > extensionPos ? -1 : extensionPos);
    }
	
	private static int indexOfLastSeparator(String filename) {
        if (filename == null) {
            return -1;
        }
        int lastUnixPos = filename.lastIndexOf("/");
        int lastWindowsPos = filename.lastIndexOf("\\");
        return Math.max(lastUnixPos, lastWindowsPos);
    }
}
